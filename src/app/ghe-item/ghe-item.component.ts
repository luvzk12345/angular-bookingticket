import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ghe-item',
  templateUrl: './ghe-item.component.html',
  styleUrls: ['./ghe-item.component.css'],
})
export class GheItemComponent implements OnInit {
  @Output() eventBooking = new EventEmitter()
  @Input() seatItem!: Seat;
   isBooked: boolean = false;
  handleBooking(seatItem:Seat) {
    this.isBooked = !this.isBooked;
    this.eventBooking.emit(seatItem);
      
  }
  constructor() {}

  ngOnInit(): void {}
}
interface Seat {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
